# Registrasi Kongres IPPNU XVIII

Project ini merupakan **backend** dari registrasi kongres ippnu xviii yg bisa dilihat [disini, kalo masih aktif :p](http://kongres.ippnu.or.id)

# Teknologi yang dipakai

- **HTML,CSS, JS**
 - [waifung0207/ci_bootstrap_3](https://github.com/waifung0207/ci_bootstrap_3) (Bootstrapper CI3, untuk CI3 yang masih imut-imut bisa dicek [disini](https://github.com/bcit-ci/CodeIgniter))
- [Intervention/image](https://github.com/Intervention/image) (Library buat manipulasi gambar, mantap djiwa pokoknyaa)

# Instalasi
- Clone repo ini
- Buka terminal, masuk ke direktori application, ketik:
> composer install

### Special Thanks
- [Kang Barok](https://www.instagram.com/mubarok.id/)

	Japri saya? [cobaniatbaik@gmail.com](mailto:cobaniatbaik@gmail.com)
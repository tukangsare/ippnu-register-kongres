#
# TABLE STRUCTURE FOR: admin_groups
#

DROP TABLE IF EXISTS `admin_groups`;

CREATE TABLE `admin_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `admin_groups` (`id`, `name`, `description`) VALUES ('1', 'webmaster', 'Webmaster');
INSERT INTO `admin_groups` (`id`, `name`, `description`) VALUES ('2', 'anggota', 'Anggota Kongres');


#
# TABLE STRUCTURE FOR: admin_login_attempts
#

DROP TABLE IF EXISTS `admin_login_attempts`;

CREATE TABLE `admin_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: admin_users
#

DROP TABLE IF EXISTS `admin_users`;

CREATE TABLE `admin_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO `admin_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`) VALUES ('1', '127.0.0.1', 'webmaster', '$2y$08$/X5gzWjesYi78GqeAv5tA.dVGBVP7C1e1PzqnYCVe5s1qhlDIPPES', NULL, NULL, NULL, NULL, NULL, NULL, '1451900190', '1544026163', '1', 'Webmaster', '');
INSERT INTO `admin_users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`) VALUES ('11', '127.0.0.1', 'ippnu-11', '$2y$08$HzMlmgTHv.Y7/dD3kFFAZOtZudwpT0O/DUzNOyeGt8OQ7G9Ld7Xxm', NULL, 'aceh@gmail.com', NULL, NULL, NULL, NULL, '1544011983', '1544019729', '1', 'PW', 'Aceh');


#
# TABLE STRUCTURE FOR: admin_users_groups
#

DROP TABLE IF EXISTS `admin_users_groups`;

CREATE TABLE `admin_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO `admin_users_groups` (`id`, `user_id`, `group_id`) VALUES ('1', '1', '1');
INSERT INTO `admin_users_groups` (`id`, `user_id`, `group_id`) VALUES ('11', '11', '2');


#
# TABLE STRUCTURE FOR: anggotas
#

DROP TABLE IF EXISTS `anggotas`;

CREATE TABLE `anggotas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `wilayah_id` int(11) unsigned DEFAULT NULL,
  `noreg` varchar(150) NOT NULL,
  `is_active` enum('0','1') NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `bukti_mandat` text,
  `bukti_pengesahan` text,
  `bukti_transfer` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode` (`noreg`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

INSERT INTO `anggotas` (`id`, `wilayah_id`, `noreg`, `is_active`, `user_id`, `bukti_mandat`, `bukti_pengesahan`, `bukti_transfer`) VALUES ('3', '1', 'ippnu-11', '1', '11', '11-mandat-05122018225804.png', '11-pengesahan-05122018225804.png', '11-transfer-05122018225804.png');


#
# TABLE STRUCTURE FOR: api_access
#

DROP TABLE IF EXISTS `api_access`;

CREATE TABLE `api_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL DEFAULT '',
  `controller` varchar(50) NOT NULL DEFAULT '',
  `date_created` datetime DEFAULT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: api_keys
#

DROP TABLE IF EXISTS `api_keys`;

CREATE TABLE `api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT '0',
  `is_private_key` tinyint(1) NOT NULL DEFAULT '0',
  `ip_addresses` text,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `api_keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES ('1', '0', 'anonymous', '1', '1', '0', NULL, '1463388382');


#
# TABLE STRUCTURE FOR: api_limits
#

DROP TABLE IF EXISTS `api_limits`;

CREATE TABLE `api_limits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `count` int(10) NOT NULL,
  `hour_started` int(11) NOT NULL,
  `api_key` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: api_logs
#

DROP TABLE IF EXISTS `api_logs`;

CREATE TABLE `api_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: detail_anggotas
#

DROP TABLE IF EXISTS `detail_anggotas`;

CREATE TABLE `detail_anggotas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `anggota_id` int(11) unsigned NOT NULL,
  `nama` varchar(150) NOT NULL,
  `jabatan` varchar(150) NOT NULL,
  `kontak` varchar(20) NOT NULL,
  `foto` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

INSERT INTO `detail_anggotas` (`id`, `anggota_id`, `nama`, `jabatan`, `kontak`, `foto`) VALUES ('2', '3', 'Rudi', 'Jabatan 1', '08109283921', '11-fotokomisi-05122018225804.jpg');
INSERT INTO `detail_anggotas` (`id`, `anggota_id`, `nama`, `jabatan`, `kontak`, `foto`) VALUES ('3', '3', 'Isan', 'Kroco 2', '08109283922', '11-fotoprogram-05122018225804.jpg');
INSERT INTO `detail_anggotas` (`id`, `anggota_id`, `nama`, `jabatan`, `kontak`, `foto`) VALUES ('4', '3', 'Intan', 'Kroco 3', '08109283923', '11-fotorekomendasi-05122018225804.jpg');
INSERT INTO `detail_anggotas` (`id`, `anggota_id`, `nama`, `jabatan`, `kontak`, `foto`) VALUES ('5', '3', 'Sopi', 'Kroco 4', '08109283924', '11-fotopeninjau-05122018225804.jpg');


#
# TABLE STRUCTURE FOR: groups
#

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `groups` (`id`, `name`, `description`) VALUES ('1', 'members', 'General User');
INSERT INTO `groups` (`id`, `name`, `description`) VALUES ('2', 'Anggota', 'Anggota');


#
# TABLE STRUCTURE FOR: kabupaten
#

DROP TABLE IF EXISTS `kabupaten`;

CREATE TABLE `kabupaten` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `prefix` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_kabupaten` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provinsi_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_foreignkey_kabupaten_provinsi` (`provinsi_id`),
  CONSTRAINT `c_fk_kabupaten_provinsi_id` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=515 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('1', 'Kab.', 'Aceh Barat', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('2', 'Kab.', 'Aceh Barat Daya', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('3', 'Kab.', 'Aceh Besar', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('4', 'Kab.', 'Aceh Jaya', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('5', 'Kab.', 'Aceh Selatan', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('6', 'Kab.', 'Aceh Singkil', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('7', 'Kab.', 'Aceh Tamiang', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('8', 'Kab.', 'Aceh Tengah', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('9', 'Kab.', 'Aceh Tenggara', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('10', 'Kab.', 'Aceh Timur', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('11', 'Kab.', 'Aceh Utara', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('12', 'Kab.', 'Agam', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('13', 'Kab.', 'Alor', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('14', 'Kota', 'Ambon', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('15', 'Kab.', 'Asahan', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('16', 'Kab.', 'Asmat', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('17', 'Kab.', 'Badung', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('18', 'Kab.', 'Balangan', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('19', 'Kota', 'Balikpapan', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('20', 'Kota', 'Banda Aceh', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('21', 'Kota', 'Bandar Lampung', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('22', 'Kota', 'Bandung', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('23', 'Kab.', 'Bandung', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('24', 'Kab.', 'Bandung Barat', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('25', 'Kab.', 'Banggai', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('26', 'Kab.', 'Banggai Kepulauan', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('27', 'Kab.', 'Banggai Laut', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('28', 'Kab.', 'Bangka', '13');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('29', 'Kab.', 'Bangka Barat', '13');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('30', 'Kab.', 'Bangka Selatan', '13');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('31', 'Kab.', 'Bangka Tengah', '13');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('32', 'Kab.', 'Bangkalan', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('33', 'Kab.', 'Bangli', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('34', 'Kota', 'Banjar', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('35', 'Kab.', 'Banjar', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('36', 'Kota', 'Banjarbaru', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('37', 'Kota', 'Banjarmasin', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('38', 'Kab.', 'Banjarnegara', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('39', 'Kab.', 'Bantaeng', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('40', 'Kab.', 'Bantul', '17');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('41', 'Kab.', 'Banyuasin', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('42', 'Kab.', 'Banyumas', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('43', 'Kab.', 'Banyuwangi', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('44', 'Kab.', 'Barito Kuala', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('45', 'Kab.', 'Barito Selatan', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('46', 'Kab.', 'Barito Timur', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('47', 'Kab.', 'Barito Utara', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('48', 'Kab.', 'Barru', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('49', 'Kota', 'Batam', '20');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('50', 'Kab.', 'Batang', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('51', 'Kab.', 'Batang Hari', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('52', 'Kota', 'Batu', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('53', 'Kab.', 'Batu Bara', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('54', 'Kota', 'Bau-Bau', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('55', 'Kota', 'Bekasi', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('56', 'Kab.', 'Bekasi', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('57', 'Kab.', 'Belitung', '13');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('58', 'Kab.', 'Belitung Timur', '13');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('59', 'Kab.', 'Belu', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('60', 'Kab.', 'Bener Meriah', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('61', 'Kab.', 'Bengkalis', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('62', 'Kab.', 'Bengkayang', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('63', 'Kota', 'Bengkulu', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('64', 'Kab.', 'Bengkulu Selatan', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('65', 'Kab.', 'Bengkulu Tengah', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('66', 'Kab.', 'Bengkulu Utara', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('67', 'Kab.', 'Berau', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('68', 'Kab.', 'Biak Numfor', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('69', 'Kab.', 'Bima', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('70', 'Kota', 'Bima', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('71', 'Kota', 'Binjai', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('72', 'Kab.', 'Bintan', '20');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('73', 'Kab.', 'Bireuen', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('74', 'Kota', 'Bitung', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('75', 'Kab.', 'Blitar', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('76', 'Kota', 'Blitar', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('77', 'Kab.', 'Blora', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('78', 'Kab.', 'Boalemo', '28');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('79', 'Kab.', 'Bogor', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('80', 'Kota', 'Bogor', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('81', 'Kab.', 'Bojonegoro', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('82', 'Kab.', 'Bolaang Mongondow', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('83', 'Kab.', 'Bolaang Mongondow Selatan', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('84', 'Kab.', 'Bolaang Mongondow Timur', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('85', 'Kab.', 'Bolaang Mongondow Utara', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('86', 'Kab.', 'Bombana', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('87', 'Kab.', 'Bondowoso', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('88', 'Kab.', 'Bone', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('89', 'Kab.', 'Bone Bolango', '28');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('90', 'Kota', 'Bontang', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('91', 'Kab.', 'Boven Digoel', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('92', 'Kab.', 'Boyolali', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('93', 'Kab.', 'Brebes', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('94', 'Kota', 'Bukittinggi', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('95', 'Kab.', 'Buleleng', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('96', 'Kab.', 'Bulukumba', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('97', 'Kab.', 'Bulungan', '29');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('98', 'Kab.', 'Bungo', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('99', 'Kab.', 'Buol', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('100', 'Kab.', 'Buru', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('101', 'Kab.', 'Buru Selatan', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('102', 'Kab.', 'Buton', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('103', 'Kab.', 'Buton Selatan', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('104', 'Kab.', 'Buton Tengah', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('105', 'Kab.', 'Buton Utara', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('106', 'Kab.', 'Ciamis', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('107', 'Kab.', 'Cianjur', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('108', 'Kab.', 'Cilacap', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('109', 'Kota', 'Cilegon', '30');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('110', 'Kota', 'Cimahi', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('111', 'Kota', 'Cirebon', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('112', 'Kab.', 'Cirebon', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('113', 'Kab.', 'Dairi', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('114', 'Kab.', 'Deiyai', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('115', 'Kab.', 'Deli Serdang', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('116', 'Kab.', 'Demak', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('117', 'Kota', 'Denpasar', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('118', 'Kota', 'Depok', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('119', 'Kab.', 'Dharmasraya', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('120', 'Kab.', 'Dogiyai', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('121', 'Kab.', 'Dompu', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('122', 'Kab.', 'Donggala', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('123', 'Kota', 'Dumai', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('124', 'Kab.', 'Empat Lawang', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('125', 'Kab.', 'Ende', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('126', 'Kab.', 'Enrekang', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('127', 'Kab.', 'Fakfak', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('128', 'Kab.', 'Flores Timur', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('129', 'Kab.', 'Garut', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('130', 'Kab.', 'Gayo Lues', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('131', 'Kab.', 'Gianyar', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('132', 'Kab.', 'Gorontalo', '28');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('133', 'Kota', 'Gorontalo', '28');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('134', 'Kab.', 'Gorontalo Utara', '28');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('135', 'Kab.', 'Gowa', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('136', 'Kab.', 'Gresik', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('137', 'Kab.', 'Grobogan', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('138', 'Kab.', 'Gunung Kidul', '17');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('139', 'Kab.', 'Gunung Mas', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('140', 'Kota', 'Gunungsitoli', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('141', 'Kab.', 'Halmahera Barat', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('142', 'Kab.', 'Halmahera Selatan', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('143', 'Kab.', 'Halmahera Tengah', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('144', 'Kab.', 'Halmahera Timur', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('145', 'Kab.', 'Halmahera Utara', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('146', 'Kab.', 'Hulu Sungai Selatan', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('147', 'Kab.', 'Hulu Sungai Tengah', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('148', 'Kab.', 'Hulu Sungai Utara', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('149', 'Kab.', 'Humbang Hasundutan', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('150', 'Kab.', 'Indragiri Hilir', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('151', 'Kab.', 'Indragiri Hulu', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('152', 'Kab.', 'Indramayu', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('153', 'Kab.', 'Intan Jaya', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('154', 'Kota', 'Jakarta Barat', '33');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('155', 'Kota', 'Jakarta Pusat', '33');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('156', 'Kota', 'Jakarta Selatan', '33');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('157', 'Kota', 'Jakarta Timur', '33');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('158', 'Kota', 'Jakarta Utara', '33');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('159', 'Kota', 'Jambi', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('160', 'Kab.', 'Jayapura', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('161', 'Kota', 'Jayapura', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('162', 'Kab.', 'Jayawijaya', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('163', 'Kab.', 'Jember', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('164', 'Kab.', 'Jembrana', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('165', 'Kab.', 'Jeneponto', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('166', 'Kab.', 'Jepara', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('167', 'Kab.', 'Jombang', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('168', 'Kab.', 'Kaimana', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('169', 'Kab.', 'Kampar', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('170', 'Kab.', 'Kapuas', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('171', 'Kab.', 'Kapuas Hulu', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('172', 'Kab.', 'Karanganyar', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('173', 'Kab.', 'Karangasem', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('174', 'Kab.', 'Karawang', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('175', 'Kab.', 'Karimun', '20');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('176', 'Kab.', 'Karo', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('177', 'Kab.', 'Katingan', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('178', 'Kab.', 'Kaur', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('179', 'Kab.', 'Kayong Utara', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('180', 'Kab.', 'Kebumen', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('181', 'Kab.', 'Kediri', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('182', 'Kota', 'Kediri', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('183', 'Kab.', 'Keerom', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('184', 'Kab.', 'Kendal', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('185', 'Kota', 'Kendari', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('186', 'Kab.', 'Kepahiang', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('187', 'Kab.', 'Kepulauan Anambas', '20');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('188', 'Kab.', 'Kepulauan Aru', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('189', 'Kab.', 'Kepulauan Mentawai', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('190', 'Kab.', 'Kepulauan Meranti', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('191', 'Kab.', 'Kepulauan Sangihe', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('192', 'Kab.', 'Kepulauan Selayar', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('193', 'Kab.', 'Kepulauan Seribu', '33');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('194', 'Kab.', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('195', 'Kab.', 'Kepulauan Sula', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('196', 'Kab.', 'Kepulauan Talaud', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('197', 'Kab.', 'Kepulauan Yapen', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('198', 'Kab.', 'Kerinci', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('199', 'Kab.', 'Ketapang', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('200', 'Kab.', 'Klaten', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('201', 'Kab.', 'Klungkung', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('202', 'Kab.', 'Kolaka', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('203', 'Kab.', 'Kolaka Timur', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('204', 'Kab.', 'Kolaka Utara', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('205', 'Kab.', 'Konawe', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('206', 'Kab.', 'Konawe Kepulauan', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('207', 'Kab.', 'Konawe Selatan', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('208', 'Kab.', 'Konawe Utara', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('209', 'Kab.', 'Kotabaru', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('210', 'Kota', 'Kotamobagu', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('211', 'Kab.', 'Kotawaringin Barat', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('212', 'Kab.', 'Kotawaringin Timur', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('213', 'Kab.', 'Kuantan Singingi', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('214', 'Kab.', 'Kubu Raya', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('215', 'Kab.', 'Kudus', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('216', 'Kab.', 'Kulon Progo', '17');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('217', 'Kab.', 'Kuningan', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('218', 'Kab.', 'Kupang', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('219', 'Kota', 'Kupang', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('220', 'Kab.', 'Kutai Barat', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('221', 'Kab.', 'Kutai Kartanegara', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('222', 'Kab.', 'Kutai Timur', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('223', 'Kab.', 'Labuhanbatu', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('224', 'Kab.', 'Labuhanbatu Selatan', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('225', 'Kab.', 'Labuhanbatu Utara', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('226', 'Kab.', 'Lahat', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('227', 'Kab.', 'Lamandau', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('228', 'Kab.', 'Lamongan', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('229', 'Kab.', 'Lampung Barat', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('230', 'Kab.', 'Lampung Selatan', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('231', 'Kab.', 'Lampung Tengah', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('232', 'Kab.', 'Lampung Timur', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('233', 'Kab.', 'Lampung Utara', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('234', 'Kab.', 'Landak', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('235', 'Kab.', 'Langkat', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('236', 'Kota', 'Langsa', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('237', 'Kab.', 'Lanny Jaya', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('238', 'Kab.', 'Lebak', '30');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('239', 'Kab.', 'Lebong', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('240', 'Kab.', 'Lembata', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('241', 'Kota', 'Lhokseumawe', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('242', 'Kab.', 'Lima Puluh Kota', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('243', 'Kab.', 'Lingga', '20');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('244', 'Kab.', 'Lombok Barat', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('245', 'Kab.', 'Lombok Tengah', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('246', 'Kab.', 'Lombok Timur', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('247', 'Kab.', 'Lombok Utara', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('248', 'Kota', 'Lubuk Linggau', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('249', 'Kab.', 'Lumajang', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('250', 'Kab.', 'Luwu', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('251', 'Kab.', 'Luwu Timur', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('252', 'Kab.', 'Luwu Utara', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('253', 'Kab.', 'Madiun', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('254', 'Kota', 'Madiun', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('255', 'Kab.', 'Magelang', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('256', 'Kota', 'Magelang', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('257', 'Kab.', 'Magetan', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('258', 'Kab.', 'Mahakam Ulu', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('259', 'Kab.', 'Majalengka', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('260', 'Kab.', 'Majene', '34');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('261', 'Kota', 'Makassar', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('262', 'Kab.', 'Malaka', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('263', 'Kab.', 'Malang', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('264', 'Kota', 'Malang', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('265', 'Kab.', 'Malinau', '29');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('266', 'Kab.', 'Maluku Barat Daya', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('267', 'Kab.', 'Maluku Tengah', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('268', 'Kab.', 'Maluku Tenggara', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('269', 'Kab.', 'Maluku Tenggara Barat', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('270', 'Kab.', 'Mamasa', '34');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('271', 'Kab.', 'Mamberamo Raya', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('272', 'Kab.', 'Mamberamo Tengah', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('273', 'Kab.', 'Mamuju', '34');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('274', 'Kab.', 'Mamuju Tengah', '34');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('275', 'Kab.', 'Mamuju Utara', '34');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('276', 'Kota', 'Manado', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('277', 'Kab.', 'Mandailing Natal', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('278', 'Kab.', 'Manggarai', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('279', 'Kab.', 'Manggarai Barat', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('280', 'Kab.', 'Manggarai Timur', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('281', 'Kab.', 'Manokwari', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('282', 'Kab.', 'Manokwari Selatan', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('283', 'Kab.', 'Mappi', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('284', 'Kab.', 'Maros', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('285', 'Kota', 'Mataram', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('286', 'Kab.', 'Maybrat', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('287', 'Kota', 'Medan', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('288', 'Kab.', 'Melawi', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('289', 'Kab.', 'Mempawah', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('290', 'Kab.', 'Merangin', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('291', 'Kab.', 'Merauke', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('292', 'Kab.', 'Mesuji', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('293', 'Kota', 'Metro', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('294', 'Kab.', 'Mimika', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('295', 'Kab.', 'Minahasa', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('296', 'Kab.', 'Minahasa Selatan', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('297', 'Kab.', 'Minahasa Tenggara', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('298', 'Kab.', 'Minahasa Utara', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('299', 'Kab.', 'Mojokerto', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('300', 'Kota', 'Mojokerto', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('301', 'Kab.', 'Morowali', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('302', 'Kab.', 'Morowali Utara', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('303', 'Kab.', 'Muara Enim', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('304', 'Kab.', 'Muaro Jambi', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('305', 'Kab.', 'Muko Muko', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('306', 'Kab.', 'Muna', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('307', 'Kab.', 'Muna Barat', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('308', 'Kab.', 'Murung Raya', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('309', 'Kab.', 'Musi Banyuasin', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('310', 'Kab.', 'Musi Rawas', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('311', 'Kab.', 'Musi Rawas Utara', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('312', 'Kab.', 'Nabire', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('313', 'Kab.', 'Nagan Raya', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('314', 'Kab.', 'Nagekeo', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('315', 'Kab.', 'Natuna', '20');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('316', 'Kab.', 'Nduga', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('317', 'Kab.', 'Ngada', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('318', 'Kab.', 'Nganjuk', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('319', 'Kab.', 'Ngawi', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('320', 'Kab.', 'Nias', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('321', 'Kab.', 'Nias Barat', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('322', 'Kab.', 'Nias Selatan', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('323', 'Kab.', 'Nias Utara', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('324', 'Kab.', 'Nunukan', '29');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('325', 'Kab.', 'Ogan Ilir', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('326', 'Kab.', 'Ogan Komering Ilir', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('327', 'Kab.', 'Ogan Komering Ulu', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('328', 'Kab.', 'Ogan Komering Ulu Selatan', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('329', 'Kab.', 'Ogan Komering Ulu Timur', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('330', 'Kab.', 'Pacitan', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('331', 'Kota', 'Padang', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('332', 'Kab.', 'Padang Lawas', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('333', 'Kab.', 'Padang Lawas Utara', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('334', 'Kota', 'Padang Panjang', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('335', 'Kab.', 'Padang Pariaman', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('336', 'Kota', 'Padang Sidempuan', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('337', 'Kota', 'Pagar Alam', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('338', 'Kab.', 'Pakpak Bharat', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('339', 'Kota', 'Palangka Raya', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('340', 'Kota', 'Palembang', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('341', 'Kota', 'Palopo', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('342', 'Kota', 'Palu', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('343', 'Kab.', 'Pamekasan', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('344', 'Kab.', 'Pandeglang', '30');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('345', 'Kab.', 'Pangandaran', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('346', 'Kab.', 'Pangkajene Kepulauan', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('347', 'Kota', 'Pangkal Pinang', '13');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('348', 'Kab.', 'Paniai', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('349', 'Kota', 'Parepare', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('350', 'Kota', 'Pariaman', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('351', 'Kab.', 'Parigi Moutong', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('352', 'Kab.', 'Pasaman', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('353', 'Kab.', 'Pasaman Barat', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('354', 'Kab.', 'Paser', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('355', 'Kab.', 'Pasuruan', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('356', 'Kota', 'Pasuruan', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('357', 'Kab.', 'Pati', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('358', 'Kota', 'Payakumbuh', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('359', 'Kab.', 'Pegunungan Arfak', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('360', 'Kab.', 'Pegunungan Bintang', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('361', 'Kab.', 'Pekalongan', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('362', 'Kota', 'Pekalongan', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('363', 'Kota', 'Pekanbaru', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('364', 'Kab.', 'Pelalawan', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('365', 'Kab.', 'Pemalang', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('366', 'Kota', 'Pematang Siantar', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('367', 'Kab.', 'Penajam Paser Utara', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('368', 'Kab.', 'Penukal Abab Lematang Ilir', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('369', 'Kab.', 'Pesawaran', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('370', 'Kab.', 'Pesisir Barat', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('371', 'Kab.', 'Pesisir Selatan', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('372', 'Kab.', 'Pidie', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('373', 'Kab.', 'Pidie Jaya', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('374', 'Kab.', 'Pinrang', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('375', 'Kab.', 'Pohuwato', '28');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('376', 'Kab.', 'Polewali Mandar', '34');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('377', 'Kab.', 'Ponorogo', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('378', 'Kota', 'Pontianak', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('379', 'Kab.', 'Poso', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('380', 'Kota', 'Prabumulih', '18');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('381', 'Kab.', 'Pringsewu', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('382', 'Kab.', 'Probolinggo', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('383', 'Kota', 'Probolinggo', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('384', 'Kab.', 'Pulang Pisau', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('385', 'Kab.', 'Pulau Morotai', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('386', 'Kab.', 'Pulau Taliabu', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('387', 'Kab.', 'Puncak', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('388', 'Kab.', 'Puncak Jaya', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('389', 'Kab.', 'Purbalingga', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('390', 'Kab.', 'Purwakarta', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('391', 'Kab.', 'Purworejo', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('392', 'Kab.', 'Raja Ampat', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('393', 'Kab.', 'Rejang Lebong', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('394', 'Kab.', 'Rembang', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('395', 'Kab.', 'Rokan Hilir', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('396', 'Kab.', 'Rokan Hulu', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('397', 'Kab.', 'Rote Ndao', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('398', 'Kota', 'Sabang', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('399', 'Kab.', 'Sabu Raijua', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('400', 'Kota', 'Salatiga', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('401', 'Kota', 'Samarinda', '9');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('402', 'Kab.', 'Sambas', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('403', 'Kab.', 'Samosir', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('404', 'Kab.', 'Sampang', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('405', 'Kab.', 'Sanggau', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('406', 'Kab.', 'Sarmi', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('407', 'Kab.', 'Sarolangun', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('408', 'Kota', 'Sawah Lunto', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('409', 'Kab.', 'Sekadau', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('410', 'Kab.', 'Seluma', '25');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('411', 'Kab.', 'Semarang', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('412', 'Kota', 'Semarang', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('413', 'Kab.', 'Seram Bagian Barat', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('414', 'Kab.', 'Seram Bagian Timur', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('415', 'Kota', 'Serang', '30');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('416', 'Kab.', 'Serang', '30');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('417', 'Kab.', 'Serdang Bedagai', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('418', 'Kab.', 'Seruyan', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('419', 'Kab.', 'Siak', '23');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('420', 'Kota', 'Sibolga', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('421', 'Kab.', 'Sidenreng Rappang', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('422', 'Kab.', 'Sidoarjo', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('423', 'Kab.', 'Sigi', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('424', 'Kab.', 'Sijunjung', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('425', 'Kab.', 'Sikka', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('426', 'Kab.', 'Simalungun', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('427', 'Kab.', 'Simeulue', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('428', 'Kota', 'Singkawang', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('429', 'Kab.', 'Sinjai', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('430', 'Kab.', 'Sintang', '24');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('431', 'Kab.', 'Situbondo', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('432', 'Kab.', 'Sleman', '17');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('433', 'Kab.', 'Solok', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('434', 'Kota', 'Solok', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('435', 'Kab.', 'Solok Selatan', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('436', 'Kab.', 'Soppeng', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('437', 'Kab.', 'Sorong', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('438', 'Kota', 'Sorong', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('439', 'Kab.', 'Sorong Selatan', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('440', 'Kab.', 'Sragen', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('441', 'Kab.', 'Subang', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('442', 'Kota', 'Subulussalam', '1');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('443', 'Kota', 'Sukabumi', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('444', 'Kab.', 'Sukabumi', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('445', 'Kab.', 'Sukamara', '19');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('446', 'Kab.', 'Sukoharjo', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('447', 'Kab.', 'Sumba Barat', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('448', 'Kab.', 'Sumba Barat Daya', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('449', 'Kab.', 'Sumba Tengah', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('450', 'Kab.', 'Sumba Timur', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('451', 'Kab.', 'Sumbawa', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('452', 'Kab.', 'Sumbawa Barat', '26');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('453', 'Kab.', 'Sumedang', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('454', 'Kab.', 'Sumenep', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('455', 'Kota', 'Sungaipenuh', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('456', 'Kab.', 'Supiori', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('457', 'Kota', 'Surabaya', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('458', 'Kota', 'Surakarta', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('459', 'Kab.', 'Tabalong', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('460', 'Kab.', 'Tabanan', '7');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('461', 'Kab.', 'Takalar', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('462', 'Kab.', 'Tambrauw', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('463', 'Kab.', 'Tana Tidung', '29');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('464', 'Kab.', 'Tana Toraja', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('465', 'Kab.', 'Tanah Bumbu', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('466', 'Kab.', 'Tanah Datar', '2');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('467', 'Kab.', 'Tanah Laut', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('468', 'Kota', 'Tangerang', '30');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('469', 'Kab.', 'Tangerang', '30');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('470', 'Kota', 'Tangerang Selatan', '30');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('471', 'Kab.', 'Tanggamus', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('472', 'Kota', 'Tanjung Balai', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('473', 'Kab.', 'Tanjung Jabung Barat', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('474', 'Kab.', 'Tanjung Jabung Timur', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('475', 'Kota', 'Tanjung Pinang', '20');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('476', 'Kab.', 'Tapanuli Selatan', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('477', 'Kab.', 'Tapanuli Tengah', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('478', 'Kab.', 'Tapanuli Utara', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('479', 'Kab.', 'Tapin', '8');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('480', 'Kota', 'Tarakan', '29');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('481', 'Kota', 'Tasikmalaya', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('482', 'Kab.', 'Tasikmalaya', '11');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('483', 'Kota', 'Tebing Tinggi', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('484', 'Kab.', 'Tebo', '21');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('485', 'Kab.', 'Tegal', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('486', 'Kota', 'Tegal', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('487', 'Kab.', 'Teluk Bintuni', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('488', 'Kab.', 'Teluk Wondama', '31');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('489', 'Kab.', 'Temanggung', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('490', 'Kota', 'Ternate', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('491', 'Kota', 'Tidore Kepulauan', '32');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('492', 'Kab.', 'Timor Tengah Selatan', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('493', 'Kab.', 'Timor Tengah Utara', '3');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('494', 'Kab.', 'Toba Samosir', '5');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('495', 'Kab.', 'Tojo Una-Una', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('496', 'Kab.', 'Toli-Toli', '12');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('497', 'Kab.', 'Tolikara', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('498', 'Kota', 'Tomohon', '27');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('499', 'Kab.', 'Toraja Utara', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('500', 'Kab.', 'Trenggalek', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('501', 'Kota', 'Tual', '4');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('502', 'Kab.', 'Tuban', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('503', 'Kab.', 'Tulang Bawang', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('504', 'Kab.', 'Tulang Bawang Barat', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('505', 'Kab.', 'Tulungagung', '14');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('506', 'Kab.', 'Wajo', '16');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('507', 'Kab.', 'Wakatobi', '22');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('508', 'Kab.', 'Waropen', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('509', 'Kab.', 'Way Kanan', '10');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('510', 'Kab.', 'Wonogiri', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('511', 'Kab.', 'Wonosobo', '15');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('512', 'Kab.', 'Yahukimo', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('513', 'Kab.', 'Yalimo', '6');
INSERT INTO `kabupaten` (`id`, `prefix`, `nama_kabupaten`, `provinsi_id`) VALUES ('514', 'Kota', 'Yogyakarta', '17');


#
# TABLE STRUCTURE FOR: login_attempts
#

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# TABLE STRUCTURE FOR: provinsi
#

DROP TABLE IF EXISTS `provinsi`;

CREATE TABLE `provinsi` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_provinsi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('1', 'Aceh (NAD)');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('2', 'Sumatera Barat');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('3', 'Nusa Tenggara Timur (NTT)');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('4', 'Maluku');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('5', 'Sumatera Utara');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('6', 'Papua');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('7', 'Bali');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('8', 'Kalimantan Selatan');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('9', 'Kalimantan Timur');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('10', 'Lampung');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('11', 'Jawa Barat');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('12', 'Sulawesi Tengah');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('13', 'Kepulauan Bangka Belitung');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('14', 'Jawa Timur');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('15', 'Jawa Tengah');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('16', 'Sulawesi Selatan');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('17', 'DI Yogyakarta');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('18', 'Sumatera Selatan');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('19', 'Kalimantan Tengah');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('20', 'Kepulauan Riau');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('21', 'Jambi');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('22', 'Sulawesi Tenggara');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('23', 'Riau');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('24', 'Kalimantan Barat');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('25', 'Bengkulu');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('26', 'Nusa Tenggara Barat (NTB)');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('27', 'Sulawesi Utara');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('28', 'Gorontalo');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('29', 'Kalimantan Utara');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('30', 'Banten');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('31', 'Papua Barat');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('32', 'Maluku Utara');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('33', 'DKI Jakarta');
INSERT INTO `provinsi` (`id`, `nama_provinsi`) VALUES ('34', 'Sulawesi Barat');


#
# TABLE STRUCTURE FOR: user_access_keys
#

DROP TABLE IF EXISTS `user_access_keys`;

CREATE TABLE `user_access_keys` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_access_key` varchar(150) NOT NULL,
  `is_used` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

INSERT INTO `user_access_keys` (`id`, `user_access_key`, `is_used`) VALUES ('1', '3a43eb773fba257b6854798696b62d26', '1');
INSERT INTO `user_access_keys` (`id`, `user_access_key`, `is_used`) VALUES ('2', '7f4f2a2872ba75f316ee4f6bf2c5d6f1', '1');


#
# TABLE STRUCTURE FOR: users
#

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES ('1', '127.0.0.1', 'member', '$2y$08$kkqUE2hrqAJtg.pPnAhvL.1iE7LIujK5LZ61arONLpaBBWh/ek61G', NULL, 'member@member.com', NULL, NULL, NULL, NULL, '1451903855', '1451905011', '1', 'Member', 'One', NULL, NULL);


#
# TABLE STRUCTURE FOR: users_groups
#

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES ('1', '1', '1');
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES ('2', '2', '1');
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES ('3', '2', '2');
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES ('4', '3', '2');
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES ('5', '4', '1');
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES ('6', '4', '2');


#
# TABLE STRUCTURE FOR: wilayahs
#

DROP TABLE IF EXISTS `wilayahs`;

CREATE TABLE `wilayahs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `is_used` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=549 DEFAULT CHARSET=utf8mb4;

INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('1', '11', 'ACEH', '1');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('2', '11.01', 'KAB. ACEH SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('3', '11.02', 'KAB. ACEH TENGGARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('4', '11.03', 'KAB. ACEH TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('5', '11.04', 'KAB. ACEH TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('6', '11.05', 'KAB. ACEH BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('7', '11.06', 'KAB. ACEH BESAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('8', '11.07', 'KAB. PIDIE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('9', '11.08', 'KAB. ACEH UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('10', '11.09', 'KAB. SIMEULUE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('11', '11.10', 'KAB. ACEH SINGKIL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('12', '11.11', 'KAB. BIREUEN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('13', '11.12', 'KAB. ACEH BARAT DAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('14', '11.13', 'KAB. GAYO LUES', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('15', '11.14', 'KAB. ACEH JAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('16', '11.15', 'KAB. NAGAN RAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('17', '11.16', 'KAB. ACEH TAMIANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('18', '11.17', 'KAB. BENER MERIAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('19', '11.18', 'KAB. PIDIE JAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('20', '11.71', 'KOTA BANDA ACEH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('21', '11.72', 'KOTA SABANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('22', '11.73', 'KOTA LHOKSEUMAWE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('23', '11.74', 'KOTA LANGSA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('24', '11.75', 'KOTA SUBULUSSALAM', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('25', '12', 'SUMATERA UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('26', '12.01', 'KAB. TAPANULI TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('27', '12.02', 'KAB. TAPANULI UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('28', '12.03', 'KAB. TAPANULI SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('29', '12.04', 'KAB. NIAS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('30', '12.05', 'KAB. LANGKAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('31', '12.06', 'KAB. KARO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('32', '12.07', 'KAB. DELI SERDANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('33', '12.08', 'KAB. SIMALUNGUN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('34', '12.09', 'KAB. ASAHAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('35', '12.10', 'KAB. LABUHANBATU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('36', '12.11', 'KAB. DAIRI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('37', '12.12', 'KAB. TOBA SAMOSIR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('38', '12.13', 'KAB. MANDAILING NATAL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('39', '12.14', 'KAB. NIAS SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('40', '12.15', 'KAB. PAKPAK BHARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('41', '12.16', 'KAB. HUMBANG HASUNDUTAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('42', '12.17', 'KAB. SAMOSIR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('43', '12.18', 'KAB. SERDANG BEDAGAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('44', '12.19', 'KAB. BATU BARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('45', '12.20', 'KAB. PADANG LAWAS UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('46', '12.21', 'KAB. PADANG LAWAS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('47', '12.22', 'KAB. LABUHANBATU SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('48', '12.23', 'KAB. LABUHANBATU UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('49', '12.24', 'KAB. NIAS UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('50', '12.25', 'KAB. NIAS BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('51', '12.71', 'KOTA MEDAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('52', '12.72', 'KOTA PEMATANGSIANTAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('53', '12.73', 'KOTA SIBOLGA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('54', '12.74', 'KOTA TANJUNG BALAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('55', '12.75', 'KOTA BINJAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('56', '12.76', 'KOTA TEBING TINGGI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('57', '12.77', 'KOTA PADANGSIDIMPUAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('58', '12.78', 'KOTA GUNUNGSITOLI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('59', '13', 'SUMATERA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('60', '13.01', 'KAB. PESISIR SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('61', '13.02', 'KAB. SOLOK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('62', '13.03', 'KAB. SIJUNJUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('63', '13.04', 'KAB. TANAH DATAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('64', '13.05', 'KAB. PADANG PARIAMAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('65', '13.06', 'KAB. AGAM', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('66', '13.07', 'KAB. LIMA PULUH KOTA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('67', '13.08', 'KAB. PASAMAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('68', '13.09', 'KAB. KEPULAUAN MENTAWAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('69', '13.10', 'KAB. DHARMASRAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('70', '13.11', 'KAB. SOLOK SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('71', '13.12', 'KAB. PASAMAN BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('72', '13.71', 'KOTA PADANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('73', '13.72', 'KOTA SOLOK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('74', '13.73', 'KOTA SAWAHLUNTO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('75', '13.74', 'KOTA PADANG PANJANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('76', '13.75', 'KOTA BUKITTINGGI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('77', '13.76', 'KOTA PAYAKUMBUH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('78', '13.77', 'KOTA PARIAMAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('79', '14', 'RIAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('80', '14.01', 'KAB. KAMPAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('81', '14.02', 'KAB. INDRAGIRI HULU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('82', '14.03', 'KAB. BENGKALIS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('83', '14.04', 'KAB. INDRAGIRI HILIR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('84', '14.05', 'KAB. PELALAWAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('85', '14.06', 'KAB. ROKAN HULU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('86', '14.07', 'KAB. ROKAN HILIR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('87', '14.08', 'KAB. SIAK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('88', '14.09', 'KAB. KUANTAN SINGINGI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('89', '14.10', 'KAB. KEPULAUAN MERANTI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('90', '14.71', 'KOTA PEKANBARU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('91', '14.72', 'KOTA DUMAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('92', '15', 'JAMBI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('93', '15.01', 'KAB. KERINCI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('94', '15.02', 'KAB. MERANGIN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('95', '15.03', 'KAB. SAROLANGUN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('96', '15.04', 'KAB. BATANGHARI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('97', '15.05', 'KAB. MUARO JAMBI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('98', '15.06', 'KAB. TANJUNG JABUNG BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('99', '15.07', 'KAB. TANJUNG JABUNG TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('100', '15.08', 'KAB. BUNGO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('101', '15.09', 'KAB. TEBO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('102', '15.71', 'KOTA JAMBI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('103', '15.72', 'KOTA SUNGAI PENUH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('104', '16', 'SUMATERA SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('105', '16.01', 'KAB. OGAN KOMERING ULU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('106', '16.02', 'KAB. OGAN KOMERING ILIR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('107', '16.03', 'KAB. MUARA ENIM', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('108', '16.04', 'KAB. LAHAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('109', '16.05', 'KAB. MUSI RAWAS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('110', '16.06', 'KAB. MUSI BANYUASIN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('111', '16.07', 'KAB. BANYUASIN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('112', '16.08', 'KAB. OGAN KOMERING ULU TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('113', '16.09', 'KAB. OGAN KOMERING ULU SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('114', '16.10', 'KAB. OGAN ILIR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('115', '16.11', 'KAB. EMPAT LAWANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('116', '16.12', 'KAB. PENUKAL ABAB LEMATANG ILIR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('117', '16.13', 'KAB. MUSI RAWAS UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('118', '16.71', 'KOTA PALEMBANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('119', '16.72', 'KOTA PAGAR ALAM', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('120', '16.73', 'KOTA LUBUK LINGGAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('121', '16.74', 'KOTA PRABUMULIH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('122', '17', 'BENGKULU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('123', '17.01', 'KAB. BENGKULU SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('124', '17.02', 'KAB. REJANG LEBONG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('125', '17.03', 'KAB. BENGKULU UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('126', '17.04', 'KAB. KAUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('127', '17.05', 'KAB. SELUMA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('128', '17.06', 'KAB. MUKO MUKO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('129', '17.07', 'KAB. LEBONG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('130', '17.08', 'KAB. KEPAHIANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('131', '17.09', 'KAB. BENGKULU TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('132', '17.71', 'KOTA BENGKULU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('133', '18', 'LAMPUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('134', '18.01', 'KAB. LAMPUNG SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('135', '18.02', 'KAB. LAMPUNG TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('136', '18.03', 'KAB. LAMPUNG UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('137', '18.04', 'KAB. LAMPUNG BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('138', '18.05', 'KAB. TULANG BAWANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('139', '18.06', 'KAB. TANGGAMUS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('140', '18.07', 'KAB. LAMPUNG TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('141', '18.08', 'KAB. WAY KANAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('142', '18.09', 'KAB. PESAWARAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('143', '18.10', 'KAB. PRINGSEWU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('144', '18.11', 'KAB. MESUJI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('145', '18.12', 'KAB. TULANG BAWANG BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('146', '18.13', 'KAB. PESISIR BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('147', '18.71', 'KOTA BANDAR LAMPUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('148', '18.72', 'KOTA METRO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('149', '19', 'KEPULAUAN BANGKA BELITUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('150', '19.01', 'KAB. BANGKA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('151', '19.02', 'KAB. BELITUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('152', '19.03', 'KAB. BANGKA SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('153', '19.04', 'KAB. BANGKA TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('154', '19.05', 'KAB. BANGKA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('155', '19.06', 'KAB. BELITUNG TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('156', '19.71', 'KOTA PANGKAL PINANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('157', '21', 'KEPULAUAN RIAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('158', '21.01', 'KAB. BINTAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('159', '21.02', 'KAB. KARIMUN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('160', '21.03', 'KAB. NATUNA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('161', '21.04', 'KAB. LINGGA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('162', '21.05', 'KAB. KEPULAUAN ANAMBAS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('163', '21.71', 'KOTA BATAM', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('164', '21.72', 'KOTA TANJUNG PINANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('165', '31', 'DKI JAKARTA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('166', '31.01', 'KAB. ADM. KEP. SERIBU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('167', '31.71', 'KOTA ADM. JAKARTA PUSAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('168', '31.72', 'KOTA ADM. JAKARTA UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('169', '31.73', 'KOTA ADM. JAKARTA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('170', '31.74', 'KOTA ADM. JAKARTA SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('171', '31.75', 'KOTA ADM. JAKARTA TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('172', '32', 'JAWA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('173', '32.01', 'KAB. BOGOR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('174', '32.02', 'KAB. SUKABUMI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('175', '32.03', 'KAB. CIANJUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('176', '32.04', 'KAB. BANDUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('177', '32.05', 'KAB. GARUT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('178', '32.06', 'KAB. TASIKMALAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('179', '32.07', 'KAB. CIAMIS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('180', '32.08', 'KAB. KUNINGAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('181', '32.09', 'KAB. CIREBON', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('182', '32.10', 'KAB. MAJALENGKA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('183', '32.11', 'KAB. SUMEDANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('184', '32.12', 'KAB. INDRAMAYU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('185', '32.13', 'KAB. SUBANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('186', '32.14', 'KAB. PURWAKARTA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('187', '32.15', 'KAB. KARAWANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('188', '32.16', 'KAB. BEKASI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('189', '32.17', 'KAB. BANDUNG BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('190', '32.18', 'KAB. PANGANDARAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('191', '32.71', 'KOTA BOGOR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('192', '32.72', 'KOTA SUKABUMI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('193', '32.73', 'KOTA BANDUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('194', '32.74', 'KOTA CIREBON', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('195', '32.75', 'KOTA BEKASI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('196', '32.76', 'KOTA DEPOK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('197', '32.77', 'KOTA CIMAHI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('198', '32.78', 'KOTA TASIKMALAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('199', '32.79', 'KOTA BANJAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('200', '33', 'JAWA TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('201', '33.01', 'KAB. CILACAP', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('202', '33.02', 'KAB. BANYUMAS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('203', '33.03', 'KAB. PURBALINGGA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('204', '33.04', 'KAB. BANJARNEGARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('205', '33.05', 'KAB. KEBUMEN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('206', '33.06', 'KAB. PURWOREJO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('207', '33.07', 'KAB. WONOSOBO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('208', '33.08', 'KAB. MAGELANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('209', '33.09', 'KAB. BOYOLALI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('210', '33.10', 'KAB. KLATEN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('211', '33.11', 'KAB. SUKOHARJO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('212', '33.12', 'KAB. WONOGIRI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('213', '33.13', 'KAB. KARANGANYAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('214', '33.14', 'KAB. SRAGEN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('215', '33.15', 'KAB. GROBOGAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('216', '33.16', 'KAB. BLORA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('217', '33.17', 'KAB. REMBANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('218', '33.18', 'KAB. PATI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('219', '33.19', 'KAB. KUDUS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('220', '33.20', 'KAB. JEPARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('221', '33.21', 'KAB. DEMAK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('222', '33.22', 'KAB. SEMARANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('223', '33.23', 'KAB. TEMANGGUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('224', '33.24', 'KAB. KENDAL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('225', '33.25', 'KAB. BATANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('226', '33.26', 'KAB. PEKALONGAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('227', '33.27', 'KAB. PEMALANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('228', '33.28', 'KAB. TEGAL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('229', '33.29', 'KAB. BREBES', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('230', '33.71', 'KOTA MAGELANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('231', '33.72', 'KOTA SURAKARTA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('232', '33.73', 'KOTA SALATIGA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('233', '33.74', 'KOTA SEMARANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('234', '33.75', 'KOTA PEKALONGAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('235', '33.76', 'KOTA TEGAL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('236', '34', 'DAERAH ISTIMEWA YOGYAKARTA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('237', '34.01', 'KAB. KULON PROGO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('238', '34.02', 'KAB. BANTUL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('239', '34.03', 'KAB. GUNUNGKIDUL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('240', '34.04', 'KAB. SLEMAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('241', '34.71', 'KOTA YOGYAKARTA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('242', '35', 'JAWA TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('243', '35.01', 'KAB. PACITAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('244', '35.02', 'KAB. PONOROGO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('245', '35.03', 'KAB. TRENGGALEK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('246', '35.04', 'KAB. TULUNGAGUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('247', '35.05', 'KAB. BLITAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('248', '35.06', 'KAB. KEDIRI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('249', '35.07', 'KAB. MALANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('250', '35.08', 'KAB. LUMAJANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('251', '35.09', 'KAB. JEMBER', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('252', '35.10', 'KAB. BANYUWANGI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('253', '35.11', 'KAB. BONDOWOSO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('254', '35.12', 'KAB. SITUBONDO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('255', '35.13', 'KAB. PROBOLINGGO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('256', '35.14', 'KAB. PASURUAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('257', '35.15', 'KAB. SIDOARJO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('258', '35.16', 'KAB. MOJOKERTO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('259', '35.17', 'KAB. JOMBANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('260', '35.18', 'KAB. NGANJUK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('261', '35.19', 'KAB. MADIUN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('262', '35.20', 'KAB. MAGETAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('263', '35.21', 'KAB. NGAWI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('264', '35.22', 'KAB. BOJONEGORO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('265', '35.23', 'KAB. TUBAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('266', '35.24', 'KAB. LAMONGAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('267', '35.25', 'KAB. GRESIK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('268', '35.26', 'KAB. BANGKALAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('269', '35.27', 'KAB. SAMPANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('270', '35.28', 'KAB. PAMEKASAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('271', '35.29', 'KAB. SUMENEP', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('272', '35.71', 'KOTA KEDIRI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('273', '35.72', 'KOTA BLITAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('274', '35.73', 'KOTA MALANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('275', '35.74', 'KOTA PROBOLINGGO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('276', '35.75', 'KOTA PASURUAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('277', '35.76', 'KOTA MOJOKERTO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('278', '35.77', 'KOTA MADIUN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('279', '35.78', 'KOTA SURABAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('280', '35.79', 'KOTA BATU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('281', '36', 'BANTEN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('282', '36.01', 'KAB. PANDEGLANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('283', '36.02', 'KAB. LEBAK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('284', '36.03', 'KAB. TANGERANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('285', '36.04', 'KAB. SERANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('286', '36.71', 'KOTA TANGERANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('287', '36.72', 'KOTA CILEGON', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('288', '36.73', 'KOTA SERANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('289', '36.74', 'KOTA TANGERANG SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('290', '51', 'BALI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('291', '51.01', 'KAB. JEMBRANA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('292', '51.02', 'KAB. TABANAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('293', '51.03', 'KAB. BADUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('294', '51.04', 'KAB. GIANYAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('295', '51.05', 'KAB. KLUNGKUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('296', '51.06', 'KAB. BANGLI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('297', '51.07', 'KAB. KARANGASEM', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('298', '51.08', 'KAB. BULELENG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('299', '51.71', 'KOTA DENPASAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('300', '52', 'NUSA TENGGARA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('301', '52.01', 'KAB. LOMBOK BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('302', '52.02', 'KAB. LOMBOK TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('303', '52.03', 'KAB. LOMBOK TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('304', '52.04', 'KAB. SUMBAWA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('305', '52.05', 'KAB. DOMPU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('306', '52.06', 'KAB. BIMA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('307', '52.07', 'KAB. SUMBAWA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('308', '52.08', 'KAB. LOMBOK UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('309', '52.71', 'KOTA MATARAM', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('310', '52.72', 'KOTA BIMA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('311', '53', 'NUSA TENGGARA TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('312', '53.01', 'KAB. KUPANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('313', '53.02', 'KAB TIMOR TENGAH SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('314', '53.03', 'KAB. TIMOR TENGAH UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('315', '53.04', 'KAB. BELU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('316', '53.05', 'KAB. ALOR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('317', '53.06', 'KAB. FLORES TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('318', '53.07', 'KAB. SIKKA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('319', '53.08', 'KAB. ENDE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('320', '53.09', 'KAB. NGADA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('321', '53.10', 'KAB. MANGGARAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('322', '53.11', 'KAB. SUMBA TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('323', '53.12', 'KAB. SUMBA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('324', '53.13', 'KAB. LEMBATA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('325', '53.14', 'KAB. ROTE NDAO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('326', '53.15', 'KAB. MANGGARAI BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('327', '53.16', 'KAB. NAGEKEO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('328', '53.17', 'KAB. SUMBA TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('329', '53.18', 'KAB. SUMBA BARAT DAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('330', '53.19', 'KAB. MANGGARAI TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('331', '53.20', 'KAB. SABU RAIJUA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('332', '53.21', 'KAB. MALAKA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('333', '53.71', 'KOTA KUPANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('334', '61', 'KALIMANTAN BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('335', '61.01', 'KAB. SAMBAS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('336', '61.02', 'KAB. MEMPAWAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('337', '61.03', 'KAB. SANGGAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('338', '61.04', 'KAB. KETAPANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('339', '61.05', 'KAB. SINTANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('340', '61.06', 'KAB. KAPUAS HULU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('341', '61.07', 'KAB. BENGKAYANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('342', '61.08', 'KAB. LANDAK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('343', '61.09', 'KAB. SEKADAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('344', '61.10', 'KAB. MELAWI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('345', '61.11', 'KAB. KAYONG UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('346', '61.12', 'KAB. KUBU RAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('347', '61.71', 'KOTA PONTIANAK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('348', '61.72', 'KOTA SINGKAWANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('349', '92', 'PAPUA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('350', '92.01', 'KAB. SORONG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('351', '92.02', 'KAB. MANOKWARI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('352', '92.03', 'KAB. FAK FAK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('353', '92.04', 'KAB. SORONG SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('354', '92.05', 'KAB. RAJA AMPAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('355', '92.06', 'KAB. TELUK BINTUNI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('356', '92.07', 'KAB. TELUK WONDAMA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('357', '92.08', 'KAB. KAIMANA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('358', '92.09', 'KAB. TAMBRAUW', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('359', '92.10', 'KAB. MAYBRAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('360', '92.11', 'KAB. MANOKWARI SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('361', '92.12', 'KAB. PEGUNUNGAN ARFAK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('362', '92.71', 'KOTA SORONG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('363', '62', 'KALIMANTAN TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('364', '62.01', 'KAB. KOTAWARINGIN BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('365', '62.02', 'KAB. KOTAWARINGIN TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('366', '62.03', 'KAB. KAPUAS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('367', '62.04', 'KAB. BARITO SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('368', '62.05', 'KAB. BARITO UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('369', '62.06', 'KAB. KATINGAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('370', '62.07', 'KAB. SERUYAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('371', '62.08', 'KAB. SUKAMARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('372', '62.09', 'KAB. LAMANDAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('373', '62.10', 'KAB. GUNUNG MAS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('374', '62.11', 'KAB. PULANG PISAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('375', '62.12', 'KAB. MURUNG RAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('376', '62.13', 'KAB. BARITO TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('377', '62.71', 'KOTA PALANGKARAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('378', '63', 'KALIMANTAN SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('379', '63.01', 'KAB. TANAH LAUT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('380', '63.02', 'KAB. KOTABARU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('381', '63.03', 'KAB. BANJAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('382', '63.04', 'KAB. BARITO KUALA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('383', '63.05', 'KAB. TAPIN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('384', '63.06', 'KAB. HULU SUNGAI SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('385', '63.07', 'KAB. HULU SUNGAI TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('386', '63.08', 'KAB. HULU SUNGAI UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('387', '63.09', 'KAB. TABALONG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('388', '63.10', 'KAB. TANAH BUMBU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('389', '63.11', 'KAB. BALANGAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('390', '63.71', 'KOTA BANJARMASIN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('391', '63.72', 'KOTA BANJARBARU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('392', '64', 'KALIMANTAN TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('393', '64.01', 'KAB. PASER', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('394', '64.02', 'KAB. KUTAI KARTANEGARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('395', '64.03', 'KAB. BERAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('396', '64.07', 'KAB. KUTAI BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('397', '64.08', 'KAB. KUTAI TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('398', '64.09', 'KAB. PENAJAM PASER UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('399', '64.11', 'KAB. MAHAKAM ULU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('400', '64.71', 'KOTA BALIKPAPAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('401', '64.72', 'KOTA SAMARINDA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('402', '64.74', 'KOTA BONTANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('403', '65', 'KALIMANTAN UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('404', '65.01', 'KAB. BULUNGAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('405', '65.02', 'KAB. MALINAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('406', '65.03', 'KAB. NUNUKAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('407', '65.04', 'KAB. TANA TIDUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('408', '65.71', 'KOTA TARAKAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('409', '71', 'SULAWESI UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('410', '71.01', 'KAB. BOLAANG MONGONDOW', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('411', '71.02', 'KAB. MINAHASA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('412', '71.03', 'KAB. KEPULAUAN SANGIHE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('413', '71.04', 'KAB. KEPULAUAN TALAUD', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('414', '71.05', 'KAB. MINAHASA SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('415', '71.06', 'KAB. MINAHASA UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('416', '71.07', 'KAB. MINAHASA TENGGARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('417', '71.08', 'KAB. BOLAANG MONGONDOW UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('418', '71.09', 'KAB. KEP. SIAU TAGULANDANG BIARO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('419', '71.10', 'KAB. BOLAANG MONGONDOW TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('420', '71.11', 'KAB. BOLAANG MONGONDOW SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('421', '71.71', 'KOTA MANADO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('422', '71.72', 'KOTA BITUNG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('423', '71.73', 'KOTA TOMOHON', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('424', '71.74', 'KOTA KOTAMOBAGU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('425', '72', 'SULAWESI TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('426', '72.01', 'KAB. BANGGAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('427', '72.02', 'KAB. POSO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('428', '72.03', 'KAB. DONGGALA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('429', '72.04', 'KAB. TOLI TOLI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('430', '72.05', 'KAB. BUOL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('431', '72.06', 'KAB. MOROWALI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('432', '72.07', 'KAB. BANGGAI KEPULAUAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('433', '72.08', 'KAB. PARIGI MOUTONG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('434', '72.09', 'KAB. TOJO UNA UNA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('435', '72.10', 'KAB. SIGI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('436', '72.11', 'KAB. BANGGAI LAUT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('437', '72.12', 'KAB. MOROWALI UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('438', '72.71', 'KOTA PALU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('439', '73', 'SULAWESI SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('440', '73.01', 'KAB. KEPULAUAN SELAYAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('441', '73.02', 'KAB. BULUKUMBA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('442', '73.03', 'KAB. BANTAENG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('443', '73.04', 'KAB. JENEPONTO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('444', '73.05', 'KAB. TAKALAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('445', '73.06', 'KAB. GOWA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('446', '73.07', 'KAB. SINJAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('447', '73.08', 'KAB. BONE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('448', '73.09', 'KAB. MAROS', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('449', '73.10', 'KAB. PANGKAJENE KEPULAUAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('450', '73.11', 'KAB. BARRU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('451', '73.12', 'KAB. SOPPENG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('452', '73.13', 'KAB. WAJO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('453', '73.14', 'KAB. SIDENRENG RAPPANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('454', '73.15', 'KAB. PINRANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('455', '73.16', 'KAB. ENREKANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('456', '73.17', 'KAB. LUWU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('457', '73.18', 'KAB. TANA TORAJA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('458', '73.22', 'KAB. LUWU UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('459', '73.24', 'KAB. LUWU TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('460', '73.26', 'KAB. TORAJA UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('461', '73.71', 'KOTA MAKASSAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('462', '73.72', 'KOTA PARE PARE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('463', '73.73', 'KOTA PALOPO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('464', '74', 'SULAWESI TENGGARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('465', '74.01', 'KAB. KOLAKA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('466', '74.02', 'KAB. KONAWE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('467', '74.03', 'KAB. MUNA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('468', '74.04', 'KAB. BUTON', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('469', '74.05', 'KAB. KONAWE SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('470', '74.06', 'KAB. BOMBANA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('471', '74.07', 'KAB. WAKATOBI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('472', '74.08', 'KAB. KOLAKA UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('473', '74.09', 'KAB. KONAWE UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('474', '74.10', 'KAB. BUTON UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('475', '74.11', 'KAB. KOLAKA TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('476', '74.12', 'KAB. KONAWE KEPULAUAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('477', '74.13', 'KAB. MUNA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('478', '74.14', 'KAB. BUTON TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('479', '74.15', 'KAB. BUTON SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('480', '74.71', 'KOTA KENDARI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('481', '74.72', 'KOTA BAU BAU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('482', '75', 'GORONTALO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('483', '75.01', 'KAB. GORONTALO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('484', '75.02', 'KAB. BOALEMO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('485', '75.03', 'KAB. BONE BOLANGO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('486', '75.04', 'KAB. PAHUWATO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('487', '75.05', 'KAB. GORONTALO UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('488', '75.71', 'KOTA GORONTALO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('489', '76', 'SULAWESI BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('490', '76.01', 'KAB. MAMUJU UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('491', '76.02', 'KAB. MAMUJU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('492', '76.03', 'KAB. MAMASA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('493', '76.04', 'KAB. POLEWALI MANDAR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('494', '76.05', 'KAB. MAJENE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('495', '76.06', 'KAB. MAMUJU TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('496', '81', 'MALUKU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('497', '81.01', 'KAB. MALUKU TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('498', '81.02', 'KAB. MALUKU TENGGARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('499', '81.03', 'KAB. MALUKU TENGGARA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('500', '81.04', 'KAB. BURU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('501', '81.05', 'KAB. SERAM BAGIAN TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('502', '81.06', 'KAB. SERAM BAGIAN BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('503', '81.07', 'KAB. KEPULAUAN ARU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('504', '81.08', 'KAB. MALUKU BARAT DAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('505', '81.09', 'KAB. BURU SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('506', '81.71', 'KOTA AMBON', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('507', '81.72', 'KOTA TUAL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('508', '82', 'MALUKU UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('509', '82.01', 'KAB. HALMAHERA BARAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('510', '82.02', 'KAB. HALMAHERA TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('511', '82.03', 'KAB. HALMAHERA UTARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('512', '82.04', 'KAB. HALMAHERA SELATAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('513', '82.05', 'KAB. KEPULAUAN SULA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('514', '82.06', 'KAB. HALMAHERA TIMUR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('515', '82.07', 'KAB. PULAU MOROTAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('516', '82.08', 'KAB. PULAU TALIABU', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('517', '82.71', 'KOTA TERNATE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('518', '82.72', 'KOTA TIDORE KEPULAUAN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('519', '91', 'PAPUA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('520', '91.01', 'KAB. MERAUKE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('521', '91.02', 'KAB. JAYAWIJAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('522', '91.03', 'KAB. JAYAPURA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('523', '91.04', 'KAB. NABIRE', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('524', '91.05', 'KAB. KEPULAUAN YAPEN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('525', '91.06', 'KAB. BIAK NUMFOR', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('526', '91.07', 'KAB. PUNCAK JAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('527', '91.08', 'KAB. PANIAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('528', '91.09', 'KAB. MIMIKA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('529', '91.10', 'KAB. SARMI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('530', '91.11', 'KAB. KEEROM', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('531', '91.12', 'KAB. PEGUNUNGAN BINTANG', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('532', '91.13', 'KAB. YAHUKIMO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('533', '91.14', 'KAB. TOLIKARA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('534', '91.15', 'KAB. WAROPEN', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('535', '91.16', 'KAB. BOVEN DIGOEL', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('536', '91.17', 'KAB. MAPPI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('537', '91.18', 'KAB. ASMAT', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('538', '91.19', 'KAB. SUPIORI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('539', '91.20', 'KAB. MAMBERAMO RAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('540', '91.21', 'KAB. MAMBERAMO TENGAH', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('541', '91.22', 'KAB. YALIMO', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('542', '91.23', 'KAB. LANNY JAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('543', '91.24', 'KAB. NDUGA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('544', '91.25', 'KAB. PUNCAK', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('545', '91.26', 'KAB. DOGIYAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('546', '91.27', 'KAB. INTAN JAYA', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('547', '91.28', 'KAB. DEIYAI', '0');
INSERT INTO `wilayahs` (`id`, `kode`, `nama`, `is_used`) VALUES ('548', '91.71', 'KOTA JAYAPURA', '0');



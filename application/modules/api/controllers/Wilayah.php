<?php 
/**
 * 
 */
class Wilayah extends Api_Controller
{
	public function load_by_kepengurusan_post()
	{
		$this->load->model('wilayah_model');
		$id = $this->input->post('id');
		$rawResult = $this->wilayah_model->get_by_kepengurusan($id);


		$filterResult = array_map(function ($arr)
		{
			return [
				'id' 	=> $arr->id,
				'nama' 	=> ucwords(strtolower($arr->nama))
			];
		}, $rawResult);		
		$this->response($filterResult);
	}
}
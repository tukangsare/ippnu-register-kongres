<?php 
/**
 * 
 */
class Wilayah_model extends MY_Model
{
	public function get_by_kepengurusan($id)
	{
		if ($id == '1') 
		{
			$this->where = [
				'LENGTH(kode)' => '2'
			];
		} elseif ($id == '2') 
		{
			$this->where = [
				'LENGTH(kode)' => '5'
			];			
		} else
		{
			return null;
		}
		return $this->select('id,nama')->get_all();
	}
}
<?php 
class Anggota_model extends MY_Model
{
	function __construct()
	{	
		parent::__construct();
		// $this->_table 	= 'anggotas';
	}
	// Jika true maka beneran udah pernah kepake wilayahnya
	// Jika false maka wilayah belum terjamah :*
	public function isWilayahUsed(array $data)
	{
		// $this->with = 'wilayah';
		$this->_table 	= 'anggotas';
		$counter =  $this->count_by([
			'wilayah_id' 	=> $data['wilayah'],
			'is_active' 	=> '1'
		]);
		if ($counter > 0) 
		{
			return $this->db->get_compiled_select();
		} else
		{
			return FALSE;
		}
	}
	public function get_wilayah($id)
	{
		$this->where 	= [];
		$this->_table 	= 'wilayahs';
		return $this->get($id);
	}
	public function insert_data($data)
	{
		$this->_table 	= 'anggotas';
		$this->insert($data);
		$this->_table 	= 'wilayahs';
		$this->where 	= [];
		$this->update($data['wilayah_id'],['is_used' => '1']);
	}
	public function update_data_no_pk($data,$user)
	{
		return $this->update($this->get_id_by_user($user),[
			'bukti_mandat' 		=> $data['mandat']['orig_name'],		
			'bukti_pengesahan' 	=> $data['pengesahan']['orig_name'],
			'bukti_transfer' 	=> $data['transfer']['orig_name'],
		]);
	}
	public function insert_detail($user_id,$data,$foto)
	{
		$list = [
			'komisi',
			'program',
			'rekomendasi',
			'peninjau'
		];
		// Insert Komisi
		foreach ($list as $value) 
		{
			$this->db->insert('detail_anggotas',[
				'anggota_id' 	=> $this->get_id_by_user($user_id),
				'nama'		 	=> $data[$value]['nama'],
				'jabatan'		=> $data[$value]['jabatan'],
				'kontak'		=> $data[$value]['kontak'],
				'foto'			=> $foto[$value]['orig_name'],
			]);
		}
	}
	public function get_id_by_user($user)
	{
		$this->_table 	= 'anggotas';
		$anggota = $this->get_by([
			'user_id' => $user,
		]);
		return $anggota->id;
	}
	public function data_uploaded($user)
	{
		$user = $this->get_by([
			'user_id' => $user
		]);
		if (is_array((array)$user) && count(array_filter($user)) == count($user)) 
		{
			return false;
		}
		$this->_table = 'detail_anggotas';
		$count_detail = $this->count_by([
			'anggota_id' => $user->anggota_id
		]);
		if ($count_detail != 4) 
		{
			return false;
		}
		return true;
	}
}
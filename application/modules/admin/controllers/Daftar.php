<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// NOTE: this controller inherits from MY_Controller instead of Admin_Controller,
// since no authentication is required
class Daftar extends MY_Controller 
{
	public function anggota()
	{
		$this->load->library('form_builder');
		$form = $this->form_builder->create_form($this->mModule.'/daftar/validate');
		// display form when no POST data, or validation failed
		$this->mViewData['form'] = $form;
		$this->mBodyClass = 'register-page';
		$this->render('register', 'empty');
	}
	public function validate()
	{
		$this->load->model('anggota_model');
		$post = $this->input->post();
		// return var_dump($this->anggota_model->isWilayahUsed($post));
		if ($this->anggota_model->isWilayahUsed($post)) 
		{
			return redirect_referrer();
		} else
		{
			return $this->createNewAnggotaCredentials($post);
		}
	}
    private function createNewAnggotaCredentials($data)
    {
    	$getWilayah = $this->anggota_model->get_wilayah($data['wilayah']);

    	$identity 		= $this->generateNoReg($getWilayah->kode);
    	$password 		= $identity;
    	$email 			= $data['email'];
    	$additional_data= $this->getAdditionalData($getWilayah);
    	$groups 		= ['2'];
		$user_id = $this->ion_auth->register($identity, $password, $email, $additional_data, $groups);
		if ($user_id)
		{

			$this->anggota_model->insert_data([
				'wilayah_id' 	=> $getWilayah->id,
				'noreg'			=> $identity,
				'is_active'		=> '1',
				'user_id'		=> $user_id
			]);
			// directly activate user
			$this->ion_auth->activate($user_id);
			$this->ion_auth->login($identity,$password);
			redirect($this->mModule.'/welcome/new');
		}
		else
		{
			// failed
			$errors = $this->ion_auth->errors();
			// $this->system_message->set_error($errors);
		}
		redirect_referrer();

		// return print_r(var_dump($additional_data));
    }
    private function generateNoReg($kode,$prefix = 'ippnu-')
    {
    	switch (strlen($kode)) 
    	{
    		case '2':
    			return $prefix.$kode;
    			break;
    		case '5':
    			return $prefix.implode('', explode('.', $kode));
    			break;
    		default:
    			return NULL;
    			break;
    	}
    }
    private function getAdditionalData($val)
    {
    	return [
    		'first_name' 	=> strlen($val->kode) == 2?'PW':'PC',
    		'last_name'		=> ucwords(strtolower($val->nama)),
    	];
    }
}
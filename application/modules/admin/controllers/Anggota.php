<?php 
/**
 * 
 */
class Anggota extends Admin_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$url = $this->mModule;
		if ($this->ion_auth->in_group('webmaster')) 
		{
			$url .= '/anggota/all';
		} elseif ($this->ion_auth->in_group('anggota')) 
		{
			$url .= '/anggota/load';
		}
		echo $url;
	}
	public function load()
	{
		
	}
}
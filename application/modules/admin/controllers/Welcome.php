<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends Admin_Controller {

	protected $user;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->user = $this->ion_auth_model->row();
	}
	public function new()
	{
		$this->render('welcome');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// NOTE: this controller inherits from MY_Controller instead of Admin_Controller,
// since no authentication is required
class Login extends MY_Controller {

	/**
	 * Login page and submission
	 */
	public function index()
	{
		$this->load->library('form_builder');
		$form = $this->form_builder->create_form();

		if ($form->validate())
		{
			// passed validation
			$identity = $this->input->post('username');
			$password = $this->input->post('password');
			$remember = ($this->input->post('remember')=='on');
			
			if ($this->ion_auth->login($identity, $password, $remember))
			{
				// login succeed
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
				redirect($this->mModule);
			}
			else
			{
				// login failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
				refresh();
			}
		}
		
		// display form when no POST data, or validation failed
		$this->mViewData['form'] = $form;
		$this->mBodyClass = 'login-page';
		$this->render('login', 'empty');
	}
	public function a()
	{
		echo $this->mModule;
	}
	public function anggota($prefix = '')
	{
		$this->mSiteName = 'Anggota Panel';
		$this->mViewData['register_handler']['url'] 	= 'daftar/anggota';
		$this->mViewData['register_handler']['label'] 	= 'Mau Register?';
		switch ($prefix)
		{
			case 'validate':
				$this->cekLoginAnggota($this->input->post());
				break;
			default:
				$this->formLoginAnggota();
				break;
		}
	}


	private function formLoginAnggota()
	{

		$this->load->library('form_builder');
		$form = $this->form_builder->create_form('/admin/login/anggota/validate');

		// if ($form->validate())
		// {
		// 	// passed validation
		// 	$accessKey 	= $this->input->post('access_key');
		// 	$identity 	= $this->input->post('username');
		// 	$password 	= $this->input->post('password');
		// 	$remember 	= ($this->input->post('remember')=='on');
		// 	echo $accessKey;
		// 	// if ($accessKey === 'akusukakamu') 
		// 	// {
				
		// 	// }
		// 	// if ($this->ion_auth->login($identity, $password, $remember))
		// 	// {
		// 	// 	// login succeed
		// 	// 	$messages = $this->ion_auth->messages();
		// 	// 	$this->system_message->set_success($messages);
		// 	// 	redirect($this->mModule);
		// 	// }
		// 	// else
		// 	// {
		// 	// 	// login failed
		// 	// 	$errors = $this->ion_auth->errors();
		// 	// 	$this->system_message->set_error($errors);
		// 	// 	refresh();
		// 	// }
		// }
		
		// display form when no POST data, or validation failed
		$this->mViewData['form'] = $form;
		$this->mBodyClass = 'login-page';
		$this->render('anggota-login', 'empty');
	}
	private function cekLoginAnggota($post)
	{

		$this->load->model('anggota_login_model');

		// Cek access key
		$validateAccessKey = $this->anggota_login_model->validateAccessKey($post['access_key']);
		if (!$validateAccessKey) 
		{
			return redirect_referrer();
		}
	}
	public function gen($string)
	{
		return print(md5('ippnu-'.$string));
	}
	public function cek()
	{
		// $this->load->database();
		$a = $this->db->get('user_access_keys');
		if (!empty($a)) 
		{
			return print_r($a);
		} else
		{
			return print_r("GA ADA");
		}
	}
}

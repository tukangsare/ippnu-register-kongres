<?php 
/**
 * 
 */
class Syarat extends Admin_Controller
{
	protected $post;
	protected $uploadPath;
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->model('anggota_model');
		$this->post = $this->input->post();
		$this->uploadPath = FCPATH.'uploads/images/foto/';
	}
	function uploader()
	{
		if ($this->anggota_model->data_uploaded($this->ion_auth_model->row()->id)) 
		{
			return redirect($this->mModule.'/syarat/list_anggota');
		}
		$form = $this->form_builder->create_form($this->mModule.'/syarat/upload',TRUE);
		if ($this->ion_auth->in_group('anggota')) 
		{
			$this->mViewData['form'] = $form;
			$this->mBodyClass = 'uploader-page';
			$this->render('syarat/uploader');
		}
	}
	public function upload()
	{
		// die(print(json_encode($this->uploadPath)));
		$bukti['mandat'] 	= $this->ImageUploader('mandat',$this->setImageName('mandat'),$this->uploadPath);
		$bukti['pengesahan']= $this->ImageUploader('pengesahan',$this->setImageName('pengesahan'),$this->uploadPath);
		$bukti['transfer'] 	= $this->ImageUploader('transfer',$this->setImageName('transfer'),$this->uploadPath);
		
		$foto['komisi'] 	= $this->ImageUploader('komisifoto',$this->setImageName('fotokomisi'),$this->uploadPath);
		$foto['program'] = $this->ImageUploader('programfoto',$this->setImageName('fotoprogram'),$this->uploadPath);
		$foto['rekomendasi'] = $this->ImageUploader('rekomendasifoto',$this->setImageName('fotorekomendasi'),$this->uploadPath);
		$foto['peninjau'] = $this->ImageUploader('peninjaufoto',$this->setImageName('fotopeninjau'),$this->uploadPath);
		

		// Jika Upload Foto gagal maka balik ke halaman tadi
		if (!$this->isPhotoUploaded($bukti) || !$this->isPhotoUploaded($foto)) 
		{
			return redirect_referrer();
		}

		// Logic for successfull photo upload
		// die(var_dump($bukti));
		// Upload Bukti
		$updateBukti 	= $this->anggota_model->update_data_no_pk($bukti,$this->ion_auth_model->row()->id);
		$insertDetail 	= $this->anggota_model->insert_detail($this->ion_auth_model->row()->id,$this->post,$foto);
		return redirect_referrer();
		
		// if ($updateBukti && $insertDetail) 
		// {
		// 	return redirect_referrer();
		// }

	}
	public function list_anggota()
	{
		echo "Cuk";		
	}
	private function rules($item)
	{
		$this->load->config('image_uploader_rules');
		return $this->config->item($item,'image_uploader_rules');
	}
	private function setImageName($name)
	{
		return $this->ion_auth_model->row()->id.'-'.$name.'-'.date('dmYHis');
	}
	private function ImageUploader($imgFile,$newFileName,$uploadPath,$settings = array())
	{
		if (empty($settings)) 
		{
			$this->load->config('image_uploader_rules');
			$settings = $this->rules('gambar')['rules'];
		}

		// if (is_array($imgFile)) 
		// {
		// 	$imgFile = array_map(function ($ar,$val)
		// 	{
		// 		return [
		// 			'foto' => [
		// 				$val => $ar[$imgFile['file']]
		// 			]
		// 		];
		// 	}, $imgFile['nama'],array_keys($_FILES[$imgFile['nama']]));
		// }
		$settings['file_name'] 		= $newFileName;
		$settings['upload_path'] 	= $uploadPath;
		$this->load->library('upload');
		$this->upload->initialize($settings);
		if ( ! $this->upload->do_upload($imgFile))
		{
			$data = NULL;
		}
		else
		{
			$data = $this->upload->data();
		}
		return $data;
	}
	private function isPhotoUploaded($data)
	{
		foreach ($data as $key => $value) 
		{
			if (is_null($value)) 
			{
				return FALSE;
			}
		}
		return TRUE;
	}
}
<div class="login-box">

	<div class="login-logo"><b><?php echo $site_name; ?></b></div>

	<div class="login-box-body">
		<p class="login-box-msg">Sign in to start your session</p>
		<?php echo $form->open(); ?>
			<?php echo $form->messages(); ?>
			<?php echo $form->bs3_text('Access Key', 'access_key',''); ?>
			<?php echo $form->bs3_dropdown('Kepengurusan','kepengurusan', [
				''	=> 'Silahkan Pilih Kepengurusan',
				'1' => 'Pengurus Wilayah',
				'2' => 'Pengurus Cabang'
			]); ?>
			<?php echo $form->bs3_text('No. Reg. Daerah', 'no_reg', ENVIRONMENT==='development' ? 'webmaster' : ''); ?>
			<?php echo $form->bs3_password('Password', 'password', ENVIRONMENT==='development' ? 'webmaster' : ''); ?>
			<div class="row">
				<div class="col-xs-8">
				</div>
				<div class="col-xs-4">
					<?php echo $form->bs3_submit('Sign In', 'btn btn-primary btn-block btn-flat'); ?>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-xs-12">
					<a href="<?php echo $register_handler['url'] ?>" class="pull-right"><?php echo $register_handler['label'] ?></a>
				</div>
			</div>
		<?php echo $form->close(); ?>
	</div>
</div>
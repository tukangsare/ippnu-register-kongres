<div class="login-box">
	<div class="login-logo"><b><?php echo $site_name; ?></b></div>
	<div class="login-box-body">
		<p class="login-box-msg">Register Page</p>
		<?php echo $form->open(); ?>
			<?php echo $form->messages(); ?>
			<?php echo $form->bs3_dropdown('Kepengurusan', 'kepengurusan',[
				'' 	=> 'Pilih Kepengurusan',
				'1' => 'Pengurus Wilayah',
				'2' => 'Pengurus Cabang',
			],'',['id' => 'kepengurusan']); ?>
			<?php echo $form->bs3_dropdown(
				'Daerah', // Label 
				'wilayah', // name
				[
					'' 	=> 'Pilih Daerah',
				], // opsi pilihan
				'', //selected value
				[
					'id' => 'daerah' //extra-data
				]); ?>
			<?php echo $form->bs3_email('Email', 'email', '', ['placeholder' => 'Contoh: akusukakamu@gmail.com']); ?>
			<div class="row">
				<div class="col-xs-8"></div>
				<div class="col-xs-4">
					<?php echo $form->bs3_submit('Register', 'btn btn-primary btn-block btn-flat'); ?>
				</div>
			</div>
		<?php echo $form->close(); ?>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
	$('#kepengurusan').change(function () {
		$.ajax({
			type: 'POST',
			url: `<?php echo site_url('/api/wilayah/load_by_kepengurusan') ?>`,
			data: {
				'id' : this.value
			},
			dataType: 'json'
		}).done(function (data) 
		{
			$('#daerah').find('option').not(':first').remove();
			$.each(data, function (key, value) 
			{
				$('#daerah').append("<option value='"+value.id+"'>"+value.nama+"</option>");
			});
		});
	});
});
</script>
<!-- SURAT MANDAT -->
<!-- COPY SURAT PENGESAHAN -->
<!-- BUKTI TRANSFER BIAYA REGISTRASI -->
<div class="panel">
		<?php echo $form->open() ?>
	<div class="panel-header">
		<h3 class="panel-heading">Bukti</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>Surat Mandat</label>
					<input type="file" name="mandat" placeholder="Masukka Bukti" class="form-control" asu>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Copy Surat Pengesahan</label>
					<input type="file" name="pengesahan" placeholder="Masukka Bukti" class="form-control" asu>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Bukti Transfer Biaya Registrasi</label>
					<input type="file" name="transfer" placeholder="Masukka Bukti" class="form-control" asu>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-header">
		<h3 class="panel-heading">Utusan</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-2">
				<div class="row">
					<div class="col-md-12">
						<label>Bagian</label>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="komisi[bagian]" class="form-control" placeholder="Masukkan Nama" value="KOMISI" disabled="">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="program[bagian]" class="form-control" placeholder="Masukkan Nama" value="PROGRAM" disabled="">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="rekomendasi[bagian]" class="form-control" placeholder="Masukkan Nama" value="REKOMENDASI" disabled="">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="peninjau[bagian]" class="form-control" placeholder="Masukkan Nama" value="EMPAT" disabled="">
						</div>
					</div>
				</div>
			</div>
			<!-- Nama -->
			<div class="col-md-3">
				<div class="row">
					<div class="col-md-12">
						<label>Nama Utusan</label>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="komisi[nama]" class="form-control" placeholder="Masukkan Nama" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="program[nama]" class="form-control" placeholder="Masukkan Nama" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="rekomendasi[nama]" class="form-control" placeholder="Masukkan Nama" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="peninjau[nama]" class="form-control" placeholder="Masukkan Nama" asu>
						</div>
					</div>
				</div>
			</div>

			<!-- Jabatan -->
			<div class="col-md-2">
				<div class="row">
					<div class="col-md-12">
						<label>Jabatan</label>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="komisi[jabatan]" class="form-control" placeholder="Masukkan Jabatan" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="program[jabatan]" class="form-control" placeholder="Masukkan Jabatan" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="rekomendasi[jabatan]" class="form-control" placeholder="Masukkan Jabatan" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="peninjau[jabatan]" class="form-control" placeholder="Masukkan Jabatan" asu>
						</div>
					</div>
				</div>
			</div>

			<!-- Kontak -->
			<div class="col-md-2">
				<div class="row">
					<div class="col-md-12">
						<label>Kontak</label>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="komisi[kontak]" class="form-control" placeholder="Masukkan Kontak" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="program[kontak]" class="form-control" placeholder="Masukkan Kontak" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="rekomendasi[kontak]" class="form-control" placeholder="Masukkan Kontak" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" name="peninjau[kontak]" class="form-control" placeholder="Masukkan Kontak" asu>
						</div>
					</div>
				</div>
			</div>

			<!-- Foto -->
			<div class="col-md-3">
				<div class="row">
					<div class="col-md-12">
						<label>Foto</label>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="file" name="komisifoto" class="form-control" placeholder="Masukkan Nama" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="file" name="programfoto" class="form-control" placeholder="Masukkan Nama" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="file" name="rekomendasifoto" class="form-control" placeholder="Masukkan Nama" asu>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="file" name="peninjaufoto" class="form-control" placeholder="Masukkan Nama" asu>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-7"></div>
					<div class="col-md-5">
						<?php echo $form->bs3_submit('Upload', 'btn btn-primary btn-block btn-flat'); ?>
					</div>
				</div>
			</div>

		</div>	
	</div>
	<?php echo $form->close() ?>
</div>
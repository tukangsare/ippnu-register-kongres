<?php 
$config['image_uploader_rules'] = [
	'gambar' => [
		'name' => 'gambar',
		'rules'=> [
			'allowed_types'	=> 'gif|jpg|png|jpeg',
			'max_size'		=> 100,
			'max_width'		=> 1024,
			'max_height'	=> 1024,
		],
	],
];